## Questions asked in AMA(Asked me anything) session on 16/12/2020

<br>

### 1) How to change your username and password from the local git repository ?
<br>

Ans :&nbsp; We can achieve this in two ways :

       a) Using the CLI Command from the local repo
          
          git config [--local] user.name "Full Name"
          git config [--local] user.email "email@address.com"

                 OR
                
       b) You can also edit the .git/config file manually instead to change locally.


### 2) Which command is used to find the PID of a process running on some specific port ?
<br>

Ans :&nbsp;&nbsp;&nbsp;`lsof -t -i:PORT_NUMBER`

     Where PORT_NUMBER can be any port number for e,g(3000,8080,4567)

       

### 3) How to Print some range of lines from a text file ?
<br>

Ans :&nbsp;&nbsp;&nbsp; To print the range of lines from a text file are :

      `sed -n '100,200p' file_name.txt`



### 4) What is CORS Error and what does rails do for CORS ?
<br>
Ans :&nbsp;&nbsp;Cross-Origin Resource Sharing (CORS) is a mechanism that uses additional HTTP headers to tell a browser to let a web application running at one origin (domain) have permission to access selected resources from a server at a different origin. A web application executes a cross-origin HTTP request when it requests a resource that has a different origin (domain, protocol, and port) than its own origin.
<br>
<br>
See the below image to understand:
<br>
<br>
<br>

![Client Server](https://miro.medium.com/max/521/1*Y5WOd0PqJiw3n0RmxShEpg.png)
<br>
<br>

### How rails fix the CORS Error ?
 <br>
Fixing the “CORS” error
<br>
In a nutshell, the browser is preventing the request because the frontend app is at a different origin than our backend app.
<br>
Thankfully, this is easy to fix.
<br>
Add the rack-cors gem to Gemfile and bundle install.
<br>
<br>

`gem 'rack-cors'`

Then open the file at config/initializers/cors.rb.

You’ll see that it’s completely commented out at first.
```ruby
# Rails.application.config.middleware.insert_before 0, Rack::Cors do
#   allow do
#     origins 'example.com'
#
#     resource '*',
#       headers: :any,
#       methods: [:get, :post, :put, :patch, :delete, :options, :head]
#   end
# end
```
Do NOT arbitrarily uncomment and change the origins to ‘*’… Even though that will fix the problem.

Instead, do this.

```ruby
Rails.application.config.middleware.insert_before 0, Rack::Cors do
  allow do
    origins 'http://localhost:3000'

    resource '*',
      headers: :any,
      methods: [:get]
  end
end
```
### 5) Explain flatten method in Ruby ?
<br>
Ans :&nbsp;&nbsp;Ruby’s Flatten

Say you have an array of nested, or multidimensional, arrays, that is, an array in which there are elements that are also arrays:
```ruby
array = [1, [2, 3, [4, 5]]]
```
The flatten method will return a one-dimensional array, an array where all the values are on the same level:
```ruby
array = [1, [2, 3, [4, 5]]]
array.flatten #=> [1, 2, 3, 4, 5]
```
Additionally, you can call the flatten method with an argument, which will flatten that array by that many levels:
```ruby
array = [1, [2, 3, [4, 5]]]
array.flatten(1) #=> [1, 2, 3, [4, 5]]
```

### 6) What are merge conflict in git and how we can resolve them ?
<br>
Ans :&nbsp;&nbsp;
<br>

### Merge Conflict :

Conflicts occur when the same file was changed in contradictory ways. Most modifications don't fall into this category: if two people just work on the same file, Git can most likely figure things out on its own.
The most common situation when it cannot do this is when the exact same lines were edited in that file. In that case, Git has no way of knowing what's correct - you'll have to look at the changes and decide how you want the file to finally look.

To see how the conflict in a file looks like(see below):

![](https://www.git-tower.com/learn/git/faq/solve-merge-conflicts/01-conflict-markers.png)
<br>
<br>
Your job now is to condition the file to its desired state. There are a couple of ways to do this:

(a) You can simply open the file in an editor, search for the conflict markers (see above image) and make any necessary modifications. When you're done, the file needs to look exactly as you want it to look.

(b) Alternatively, you can tell Git that you'll simply go with one of the edited versions, called "ours" or "theirs".

`git checkout --ours path/to/conflict-file.css`

Note that there are lots of dedicated "Merge Tool" applications that help you with this process. Especially in complex situations with multiple conflicts in the same file, a good tool can be of tremendous value.


### 7) What is Event bubbling and capturing in JavaScript ?
<br>
Ans :

### Event Bubbling:

Event Bubbling is the event starts from the deepest element or target element to its parents, then all its ancestors which are on the way to bottom to top. At present, all the modern browsers have event bubbling as the default way of event flow.


Consider an example on Event Bubbling :

```HTML
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width">
  <title>Event Bubbling</title>
</head>
<body>
  <div id="parent">
    <button id="child">Child</button>
  </div>
  
  <script>
    var parent = document.querySelector('#parent');
    <!-- Add click event on parent div -->
      parent.addEventListener('click', function(){
        console.log("Parent clicked");
      });

    var child = document.querySelector('#child');
    <!-- Add click event on child button -->
      child.addEventListener('click', function(){
        console.log("Child clicked");
      });
  </script>
</body>
</html>
```

### Event Capturing :

Event Capturing is the event starts from top element to target element. Modern browser doesn’t support event capturing by default but we can achieve that by code in JavaScript.

```HTML
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width">
  <title>Event Capturing</title>
</head>
<body>
  <div id="parent">
    <button id="child">Child</button>
  </div>
  
  <script>
    var parent = document.querySelector('#parent');
    var child = document.querySelector('#child');

    parent.addEventListener('click', function(){
      console.log("Parent clicked");
    },true);


    child.addEventListener('click', function(){
      console.log("Child clicked");
    });
  </script>
</body>
</html>
```

### 8) What are Rails callbacks ?
<br>
Ans :Callbacks are methods that get called at certain moments of an object's life cycle. With callbacks it is possible to write code that will run whenever an Active Record object is created, saved, updated, deleted, validated, or loaded from the database.
<br>
<br>
<br>
Different types of callbacks availaible in Rails :
<br>

### after_commit

Called after a new or existing object is committed to the database. An :on argument can be used to specify which action (create, update, or destroy) this callback should apply to.

### after_create

Called after a new object is saved.

### after_destroy

Called after an existing object is destroyed.

### after_rollback

Called after a create, update, or destroy action is rolled back on a new or existing object.

### after_save

Called after a new or existing object is saved.

### after_update

Called after an existing object is saved.

### after_validation

Called after a new or existing object’s validations occur.

### around_create

Called before a new object is saved until yield is invoked within the method triggered by the callback. Calling yield causes the object to be saved and then any proceeding code in the method will execute.

### around_destroy

Called before an existing object is destroyed until yield is invoked within the method triggered by the callback. Calling yield causes the object to be destroyed and then any proceeding code in the method will execute.

### around_save

Called before a new or existing object is saved until yield is invoked within the method triggered by the callback. Calling yield causes the object to be saved and then any proceeding code in the method will execute.

### around_update

Called before an existing object is saved until yield is invoked within the method triggered by the callback. Calling yield causes the object to be saved and then any proceeding code in the method will execute.

### before_create

Called before a new object is saved.

### before_destroy

Called before an existing object is destroyed.

### before_save

Called before a new or existing object is saved.

### before_update

Called before an existing object is saved.

### before_validation

Called before a new or existing object’s validations occur.


## Last Question I will discuss when I will take a session thoroughly
<br>
Thank you, guys :smiley:




